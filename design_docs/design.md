# Yeti Tracks

**Overview:**
A 2D top-down pursuit game. The aim is to find the tracks of the cryptoid Yeti, deep in the canyons of the Himalayan plateau. 


## Aim of the Game

The Yeti has been incredibly hard for humans to find and photograph. Your
mission is to search the snow-capped plateau of Himalayas, in search of
definitive evidence of the creature, specifically its tracks. 

Follow the tracks before they are covered by falling snow or snowdrifts. As
time goes on, the older tracks vanish, so you'll need to cover ground quickly.
Bear in mind however that you can't move too quickly, as you'll exhaust
yourself, and more importantly, you may trigger an avalanche.

## Constraints

If you walk too slowly, you'll probably not cover sufficient terrain in time to find any Yeti Tracks. 

If you walk too slowly when you've found any Yeti tricks, they may very well be smothered by snow before you follow them to find the Yeti. 

If you stop and don't walk at all, you'll freeze to death because of the extreme cold. To prevent this, you'll need to pitch your tent and stay in it.

If you walk too quickly, you will exhaust yourself much faster (there is a substantial speed penalty as it's hard work moving quickly through deep snow cover.)

If you do exhaust yourself, you will have to stop and rest. How long you have to rest,  will depend on how much you've exhausted yourself. If it's long enough, you will have to pitch your tent in order to survive the cold.

If you walk too quickly and you're near mountains, there mayb be an avalanche. There is an avalanche risk guage on the screen which goes up the closer to mountains you are and the faster you go. 

A clock ticks while the player plays the game. A better score is amassed if good quality evidence (photos) are collected in the shortest amount of time. 

##

The player can move up, down, left and right with arrow or WASD keyes. 

The player has 4 points around the sprite which are Area2Ds, and when all 4 are 'entered' by avalanche snow, the player dies and will need to respawn at a random location and start the mission from scratch. 

The player has an icon for a camera and an icon for a tent. These two may also have keys bound to them. 

Pressing the P key will pause the game. 

Pressing the Enter key will take a photo of whatever is in front of the player. These photos are kept in a folio. A folio with sufficient pictures. A variety of Yeti tracks and good photos of the Yeti will amass more points for the player. 


## Yeti AI

The Yeti is always spawned randomly in a section of the map which isn't near
the player. 

The Yeti then wanders about, travelling in the direction of half-a-dozen or so
target locations, chosen randomly. When it reaches on, it stays there for a
random amount of time, the wanders off to the next ramdon location.

As the Yeti walks, it leaves a trail of tracks, in the direction in which it's walking. These tracks, are then timed to 'dissolve', based on the current level of snowfall (which falls randoml) and also can be covered by any random snow-drifts. 

When the Yeti sees the player, it will always turn and head in the direction of a target location which is in the other end of the map, away from the player. 

The Yeti walks faster than the player, so the only way the player can 'catch' the Yeti, with sufficient time to take a photo, is by creeping up on it while it's resting in one of the target locations. 


## Avalanches

An avalanche will be a set of animated/tweened sprites which cascade down the mountains at rapid speed. In most circumstances, too fast for the player to outrun them. 

The avalanches are triggered by the player moving too close and too quickly past the mountain. 


 
