# Yeti-Tracks

A 2D, top-down game. Roam the Himalayas in search of the Yeti, the Abominable Snowman. Made for the Cryptid game jam ( https://itch.io/jam/cryptid-jam )
