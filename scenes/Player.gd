extends KinematicBody2D

onready var TIPTOE_SOUND = preload("res://assets/sounds/walking_tiptoe.wav")
onready var WALK_SOUND = preload("res://assets/sounds/walking_walk.wav")
onready var RUN_SOUND = preload("res://assets/sounds/walking_run.wav")

export var speed = 80
export var max_vel = 3
enum DIRECTIONS {up, down, left, right}
enum WALK_PACE {tiptoe = 1, walk = 2, run = 3}

var last_direction 
var vel_rate = Vector2()

var key_pressed_recently = false

var velocity = Vector2()

func _ready():
	#make sure we don't have the dust cloud active when we're stationary
	$DustCloud.visible = false
	add_to_group("player")

func _physics_process(delta):
	get_input()
	move_and_collide(velocity * delta)

func get_input():
	if !key_pressed_recently:
		#We process movement keys
		if Input.is_action_pressed("ui_right"):
			if last_direction == DIRECTIONS.right:
				#we're going right already
				vel_rate.x += 1
				$DustCloud.ChangePace(vel_rate.x)
			else:
				vel_rate.x = 1
				vel_rate.y = 0
				$AnimatedSprite.animation = "walk_right"
				last_direction = DIRECTIONS.right
				$DustCloud.ChangeDirection(DIRECTIONS.left)
				$DustCloud.ChangePace(vel_rate.x)
			vel_rate.x = clamp(vel_rate.x, -max_vel, max_vel)
			SetWalkAnimation(last_direction, vel_rate.x)
			SetWalkSound(vel_rate.x)
			key_pressed_recently = true
			$KeyTimer.start()
			
		if Input.is_action_pressed("ui_left"):
			if last_direction == DIRECTIONS.left:
				#we're going direction already
				vel_rate.x -= 1
				$DustCloud.ChangePace(vel_rate.x)
			else:
				last_direction = DIRECTIONS.left
				vel_rate.x = -1
				vel_rate.y = 0
				$AnimatedSprite.animation = "walk_left"
				$DustCloud.ChangeDirection(DIRECTIONS.right) #We want the dust to go in the opposite direction
				$DustCloud.ChangePace(vel_rate.x)
			vel_rate.x = clamp(vel_rate.x, -max_vel, max_vel)
			SetWalkAnimation(last_direction, vel_rate.x)
			SetWalkSound(vel_rate.x)
			key_pressed_recently = true
			$KeyTimer.start()
			
		if Input.is_action_pressed("ui_down"):
			if last_direction == DIRECTIONS.down:
				#we're going direction already
				vel_rate.y += 1
				$DustCloud.ChangePace(vel_rate.y)
			else:
				$AnimatedSprite.animation = "walk_down"
				last_direction = DIRECTIONS.down
				vel_rate.x = 0
				vel_rate.y = 1
				$DustCloud.ChangeDirection(DIRECTIONS.up)
				$DustCloud.ChangePace(vel_rate.y)
			vel_rate.y = clamp(vel_rate.y, -max_vel, max_vel)
			SetWalkAnimation(last_direction, vel_rate.y)
			SetWalkSound(vel_rate.y)
			key_pressed_recently = true
			$KeyTimer.start()
			
		if Input.is_action_pressed("ui_up"):
			if last_direction == DIRECTIONS.up:
				#we're going direction already
				vel_rate.y -= 1
				$DustCloud.ChangePace(vel_rate.y)
			else:
				$AnimatedSprite.animation = "walk_up"
				last_direction = DIRECTIONS.up
				vel_rate.x = 0
				vel_rate.y = -1
				$DustCloud.ChangeDirection(DIRECTIONS.down)
				$DustCloud.ChangePace(vel_rate.y)
			vel_rate.y = clamp(vel_rate.y, -max_vel, max_vel)
			SetWalkAnimation(last_direction, vel_rate.y)
			SetWalkSound(vel_rate.y)
			key_pressed_recently = true
			$KeyTimer.start()
			
	if Input.is_action_pressed("ui_accept"): 
		#User pressed SPACE bar
		vel_rate = Vector2(0,0)
		velocity =  speed * vel_rate
		SetIdleAnimation()
		$AnimatedSprite.play()
		
	#print("velocity: " + str(velocity))
	#print("vel_rate: " + str(vel_rate))
	if vel_rate.length() > 0:
		vel_rate.x = clamp(vel_rate.x, -max_vel, max_vel)
		vel_rate.y = clamp(vel_rate.y, -max_vel, max_vel)
		velocity =  speed * vel_rate
		$DustCloud.visible = true
		if !$AnimatedSprite.is_playing():
			$AnimatedSprite.play()
		if !$WalkSounds.is_playing():
			#print("$WalkSounds.play()")
			$WalkSounds.play()


func PlayerJump(object_height):
	#We're jumping off the object. We do that by walking left, right or down
	#if we're walking up, then we 'jump' to behind the object and appear a little away from the top of the object
	#print("PlayerJump(object_height): " + str(object_height))
	match last_direction:
		DIRECTIONS.up:
			self.position.y = self.position.y - 20
		DIRECTIONS.left:
			self.position = self.position + Vector2(-20, object_height)
		DIRECTIONS.down:
			self.position = self.position + Vector2(0, object_height)
		DIRECTIONS.right:
			self.position = self.position + Vector2(20, object_height)
			
			
func SetIdleAnimation():
	#print("SetIdleAnimation(): " + str(velocity))
	key_pressed_recently = true
	$KeyTimer.start()
	vel_rate = Vector2(0,0)
	velocity =  speed * vel_rate
	$DustCloud.visible = false
	$WalkSounds.stop()
	match last_direction:
		DIRECTIONS.up:
			$AnimatedSprite.animation = "idle_up"
		DIRECTIONS.left:
			$AnimatedSprite.animation = "idle_left"
		DIRECTIONS.down:
			$AnimatedSprite.animation = "idle_down"
		DIRECTIONS.right:
			$AnimatedSprite.animation = "idle_right"

func SetWalkSound(walk_pace):
	walk_pace = int(abs(walk_pace))
	$WalkSounds.stop()
	match walk_pace:
		WALK_PACE.tiptoe:
			$WalkSounds.stream = TIPTOE_SOUND
			$WalkSounds.pitch_scale = 1
			$WalkSounds.volume_db = -14
		WALK_PACE.walk:
			$WalkSounds.stream = WALK_SOUND
			$WalkSounds.pitch_scale = 1.5
			$WalkSounds.volume_db = -12
		WALK_PACE.run:
			$WalkSounds.stream = RUN_SOUND
			$WalkSounds.pitch_scale = 2.5
			$WalkSounds.volume_db = -10

func SetWalkAnimation(direction, walk_pace):
	#print("SetWalkAnimation(direction, walk_pace): " + str(walk_pace))
	walk_pace = int(abs(walk_pace))
	match direction:
		DIRECTIONS.up:
			match walk_pace:
				WALK_PACE.tiptoe:
					$AnimatedSprite.animation = "tiptoe_up"
				WALK_PACE.walk:
					$AnimatedSprite.animation = "walk_up"
				WALK_PACE.run:
					$AnimatedSprite.animation = "run_up"
		DIRECTIONS.left:
			match walk_pace:
				WALK_PACE.tiptoe:
					$AnimatedSprite.animation = "tiptoe_left"
				WALK_PACE.walk:
					$AnimatedSprite.animation = "walk_left"
				WALK_PACE.run:
					$AnimatedSprite.animation = "run_left"
		DIRECTIONS.down:
			match walk_pace:
				WALK_PACE.tiptoe:
					$AnimatedSprite.animation = "tiptoe_down"
				WALK_PACE.walk:
					$AnimatedSprite.animation = "walk_down"
				WALK_PACE.run:
					$AnimatedSprite.animation = "run_down"
		DIRECTIONS.right:
			match walk_pace:
				WALK_PACE.tiptoe:
					$AnimatedSprite.animation = "tiptoe_right"
				WALK_PACE.walk:
					$AnimatedSprite.animation = "walk_right"
				WALK_PACE.run:
					$AnimatedSprite.animation = "run_right"
			
func _on_KeyTimer_timeout():
	#This is called a short time after a key-press event. It clears the key_press mutex flag
	key_pressed_recently = false	
